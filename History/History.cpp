/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "History.hpp"
#include "MicroDB.hpp"

History::History() {

    storeHistory = true;    // clipSett->value( "StoreHistory" );
    maxHistorySize = 27;    // clipSett->value( "HistorySize" );

    clipDB = new MicroDB( QDir::home().filePath( ".config/DesQ/clipboard.db" ) );

    restoreHistory();
};

History::~History() {

    if ( storeHistory )
        saveHistory();
};

void History::addItem( QMimeData *data ) {

    clipboardData.prepend( data );

    qDebug() << "Adding" << data->formats();
    qDebug() << data->text() << "\n";

    if ( maxHistorySize > 0 )
        while ( clipboardData.count() >= maxHistorySize )
            clipboardData.removeLast();

    if ( storeHistory )
        saveHistory();
};

void History::removeItem( int pos ) {

    if ( clipboardData.count() >= pos )
        return;

    clipboardData.removeAt( pos );
};

void History::moveItemToTop( int pos ) {

    if ( clipboardData.count() >= pos )
        return;

    QMimeData *top = clipboardData.takeAt( pos );
    clipboardData.prepend( top );

    if ( storeHistory )
        saveHistory();
};

QMimeData* History::itemAt( int i ) {

    if ( i >= clipboardData.count() )
        return nullptr;

    if ( i >= maxHistorySize )
        return nullptr;

    return clipboardData.at( i );
};

QList<QMimeData *> History::items() {

    return clipboardData;
};

uint History::count() {

    return ( uint )clipboardData.count();
};

void History::saveHistory() {

    clipDB->clearDatabase();

    for( QMimeData *md: clipboardData ) {
        qDebug() << "Storing" << md->formats() << md->text();
        for( QString fmt: md->formats() ) {
            MicroDBEntry entry;

            entry.index = clipboardData.indexOf( md );
            entry.mimeType = fmt;

            QByteArray data;
            /* QImage */
            if ( fmt == "application/x-qt-image" ) {
                QImage img = qvariant_cast<QImage>( md->imageData() );
                data = QByteArray::fromRawData( ( const char * )img.bits(), img.sizeInBytes() );
            }

            else {
                data = md->data( fmt );
                if ( data.isEmpty() ) {
                    qDebug() << "Unable to save data of type" << fmt;
                    continue;
                }
            }

            entry.size = data.size();
            entry.data = data;

            /* Write the data */
            clipDB->writeEntry( entry );
        }
    }
};

void History::restoreHistory() {

    for( int i = 0; i < maxHistorySize; i++ ) {
        MicroDBEntries entries = clipDB->readEntry( i );
        QMimeData *md = new QMimeData();
        for( MicroDBEntry entry: entries ) {
            if ( entry.mimeType == "application/x-qt-image" )
                md->setImageData( QImage::fromData( entry.data ) );

            /* If we have an image, store it as both application/x-qt-image and raw data */
            else if ( entry.mimeType.startsWith( "image/" ) ) {
                md->setData( entry.mimeType, entry.data );
                md->setImageData( QImage::fromData( entry.data ) );
            }

            else if ( entry.mimeType == "text/html" ) {
                md->setData( entry.mimeType, entry.data );
                md->setHtml( QString::fromUtf8( entry.data ) );
            }

            else if ( entry.mimeType.startsWith( "text/" ) ) {
                md->setData( entry.mimeType, entry.data );
                md->setText( QString::fromUtf8( entry.data ) );
            }

            else
                md->setData( entry.mimeType, entry.data );
        }

        /* If @md was populated, add it to the history */
        if ( md->formats().count() )
            clipboardData.push_back( md );
    }
};
