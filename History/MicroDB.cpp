/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "MicroDB.hpp"

static inline QByteArray int2hex( quint64 number, int width ) {

    char *hex = new char[ width + 1 ];
    int size = sprintf( hex, "%llX", number );

    return "0x" + QByteArray( hex, size ).rightJustified( width, '0' );
};

MicroDB::MicroDB( QString name ) {

    dbf = new QFile( name );
    openSuccess = dbf->open( QFile::ReadWrite );
};

MicroDB::~MicroDB() {

    dbf->close();
}

bool MicroDB::isOpen() {

    return openSuccess;
};

void MicroDB::clearDatabase() {

    dbf->resize( 0 );
    dbf->flush();
};

void MicroDB::writeEntry( MicroDBEntry entry ) {

    if ( not openSuccess ) {
        qDebug() << "Database not open for writing";
        return;
    }

    /* Go to the last */
    dbf->seek( dbf->size() );

    /* Header: Index */
    dbf->write( int2hex( entry.index, 16 ) );

    /* Header: Mimetype */
    dbf->write( entry.mimeType.leftJustified( 256, ' ' ).toUtf8() );

    /* Header: Data size */
    dbf->write( int2hex( entry.size, 16 ) );

    /* Header: Buffer 220 bytes */
    dbf->write( QByteArray().leftJustified( 220, 0 ) );

    /* Write the data */
    dbf->write( entry.data );

    /* Buffer to complete the block */
    if ( entry.size % 512 > 0 ) {
        dbf->write( QByteArray( 512 - entry.size % 512, 0 ) );
    }

    dbf->flush();
};

MicroDBEntries MicroDB::readEntry( int idx ) {

    if ( not openSuccess ) {
        qDebug() << "Database not open for reading";
        return MicroDBEntries();
    }

    dbf->seek( 0 );

    MicroDBEntries chosen;

    while ( not dbf->atEnd() ) {
        MicroDBEntry entry;

        bool okID, okSize;
        entry.index = dbf->read( 18 ).simplified().toInt( &okID, 16 );
        entry.mimeType = QString::fromUtf8( dbf->read( 256 ).simplified() );
        entry.size = dbf->read( 18 ).toInt( &okSize, 16 );
        dbf->read( 220 );

        /* Read the data */
        if ( entry.index == (quint64)idx ) {
            /* Read out the data */
            entry.data = dbf->read( entry.size );

            /* Read and discard the buffer */
            dbf->read( 512 - entry.size % 512 );

            /* MicroDBEntry is complete, append ti return list */
            chosen.append( entry );
        }

        /* Skip this data */
        else {
            dbf->seek( dbf->pos() + entry.size );
            dbf->seek( dbf->pos() + 512 - entry.size % 512 );
        }
    }

    return chosen;
};

void createDummyClipboardDatabase() {

    MicroDB db( QDir::home().filePath( ".config/DesQ/clipboard.db" ) );
    db.clearDatabase();

    MicroDBEntry entry;

    // 0.  Text
    entry.index = 0;
    entry.mimeType = "text/plain";
    entry.data = QString( "This is some random text which I am typing to test clipboard." ).toUtf8();
    entry.size = entry.data.size();

    db.writeEntry( entry );

    // 1.  HTML
    entry.index = 1;
    entry.mimeType = "text/html";
    entry.data = QString(
        "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0//EN' 'http://www.w3.org/TR/REC-html40/strict.dtd'>\n"
        "<html><head><meta name='qrichtext' content='1' /><style type='text/css'>\n"
        "p, li { white-space: pre-wrap; }\n"
        "</style></head><body style=' font-family:\'Quicksand\'; font-size:9pt; font-weight:400; font-style:normal;'>\n"
        "<h3 style=' margin-top:14px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<a href='https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=&amp;ved=2ahUKEwjbjZ-2lO7uAhVTVisKHeDbDxAQFjAAegQIDBAC&amp;url=https%3A%2F%2Fstackoverflow.com%2Fquestions%2F4857188%2Fclearing-a-layout-in-qt&amp;usg=AOvVaw0a9s-_ZiVYOdFUxzhsuhZq'>"
        "<span style=' font-size:large; font-weight:600; text-decoration: underline; color:#0986d3;'>"
        "Clearing a Layout in Qt - Stack Overflow</span></a></h3>\n"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "<a href='https://www.google.com/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=&amp;ved=2ahUKEwjbjZ-2lO7uAhVTVisKHeDbDxAQFjAAegQIDBAC&amp;url=https%3A%2F%2Fstackoverflow.com%2Fquestions%2F4857188%2Fclearing-a-layout-in-qt&amp;usg=AOvVaw0a9s-_ZiVYOdFUxzhsuhZq'>"
        "<span style=' font-style:italic; text-decoration: underline; color:#0986d3;'>stackoverflow.com › "
        "questions › clearing-a-layout-in-qt</span></a></p>\n"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>8 answers</p>\n"
        "<p style=' margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;'>"
        "17-Aug-2011 — class ConfigurationWidget : public QWidget { Q_OBJECT public: ConfigurationWidget(QWidget *parent) : QWidget(parent) {}"
        " public slots: void moduleSelected(Module* m) { if(<span style=' font-style:italic;'>layout</span>()) "
        "{ while (itsLayout-&gt;count()&gt;0) { <span style=' font-style:italic;'>delete</span> itsLayout-&gt;takeAt(0); } } "
        "<span style=' font-style:italic;'>delete layout</span>(); itsLayout = new QFormLayout(this); itsLayout-&gt;addRow( ...</p>"
        "</body></html>"
    ).toUtf8();
    entry.size = entry.data.size();

    db.writeEntry( entry );

    // 2.  URL List
    entry.index = 2;
    entry.mimeType = "text/uir-list";
    entry.data = QString(
        "file:///home/cosmos/PhD\n"
        "file:///home/cosmos/Softwares\n"
        "file:///home/cosmos/Vault\n"
        "file:///home/cosmos/mailspring-1.8.0-amd64.deb\n"
    ).toUtf8();
    entry.size = entry.data.size();

    db.writeEntry( entry );
};
