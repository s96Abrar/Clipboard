/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QtCore>

/* TAR-insprited archive model to store clipboard entries */

typedef struct microdb_entry_t {
    quint64 index;                  //  18 bytes: 0xffffffffffffffff
    QString mimeType;               // 256 bytes
    quint64 size;                   //  18 bytes: 0xffffffffffffffff
    // 220 bytes null buffer
    QByteArray data;                // ================ Store the actual data
} MicroDBEntry;

typedef QList<MicroDBEntry> MicroDBEntries;

class MicroDB {

    public:
        MicroDB( QString filename );
        ~MicroDB();

        bool isOpen();

        void clearDatabase();

        void writeEntry( MicroDBEntry entry );
        MicroDBEntries readEntry( int idx );

    private:
        QFile *dbf;
        bool openSuccess;
};

void createDummyClipboardDatabase();
