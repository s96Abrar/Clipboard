/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

// Local Headers
#include "Global.hpp"
#include "Clipboard.hpp"

#include <desq/DesQUtils.hpp>

int main( int argc, char **argv ) {

	qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
	QApplication app( argc, argv );

	app.setOrganizationName( "DesQ" );
	app.setApplicationName( "DesQ Clipboard" );
	app.setApplicationVersion( VERSION_TEXT );
	app.setQuitOnLastWindowClosed( false );

//	createDummyClipboardDatabase();

	wl_display *display = DesQWayland::getWlDisplay();
	if ( not display ) {
		qDebug() << "Unable to acquire wl_display from the compositor.";
		qDebug() << "Your experience will be severly limited.";

		return 0;
	}

	DesQWaylandRegistry *registry = new DesQWaylandRegistry( display );
	QObject::connect(
		registry, &DesQWaylandRegistry::errorOccured, [=]( DesQWaylandRegistry::ErrorType et ) {
			qDebug() << "Error caused on registry" << et;
			qDebug() << "Valiantly trying to continue...";
		}
	);
	registry->initialize();

	Clipboard *clipboard = new Clipboard( registry );
	clipboard->startTray();

	return app.exec();
};
