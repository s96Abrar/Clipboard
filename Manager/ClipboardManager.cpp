/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * Original work by David Edmundson <davidedmundson@kde.org>
    * This file was taken from plasma-workspace project
    * Suitable modifications were done to suit the needs of this project
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "ClipboardManager.hpp"

#include <QDebug>
#include <QFile>
#include <QFutureWatcher>
#include <QGuiApplication>
#include <QPointer>

#include <QtWaylandClient/QWaylandClientExtension>
#include <qpa/qplatformnativeinterface.h>

#include <unistd.h>

#include "qwayland-wlr-data-control-unstable-v1.h"
#include "qwayland-wp-primary-selection-unstable-v1.h"

#include <sys/select.h>

#include "DataControl.hpp"
#include "PrimarySelection.hpp"

ClipboardManager* ClipboardManager::instance() {

    if ( !qApp || qApp->closingDown() )
        return nullptr;

    static ClipboardManager *systemClipboard = nullptr;
    if ( !systemClipboard )
            systemClipboard = new WaylandClipboard( qApp );

    return systemClipboard;
};

ClipboardManager::ClipboardManager( QObject *parent ) : QObject( parent ) {

};

WaylandClipboard::WaylandClipboard( QObject *parent ) : ClipboardManager( parent ) {

    mDataManager.reset( new DataControlDeviceManager() );
    mSelectionManager.reset( new PrimarySelectionManager() );

    connect( mDataManager.get(), &DataControlDeviceManager::activeChanged, this, [this]() {
        if ( mDataManager->isActive()) {
            QPlatformNativeInterface *native = qApp->platformNativeInterface();
            if (!native) {
                return;
            }
            auto seat = static_cast<struct ::wl_seat *>(native->nativeResourceForIntegration("wl_seat"));
            if (!seat) {
                return;
            }

            mDataDevice.reset(new DataControlDevice(mDataManager->get_data_device(seat)));

            connect(mDataDevice.get(), &DataControlDevice::receivedSelectionChanged, this, [this]() {
                emit changed(QClipboard::Clipboard);
            });
            connect(mDataDevice.get(), &DataControlDevice::selectionChanged, this, [this]() {
                emit changed(QClipboard::Clipboard);
            });
        } else {
            mDataDevice.reset();
        }
    });

    connect( mSelectionManager.get(), &PrimarySelectionManager::activeChanged, this, [this]() {
        if ( mSelectionManager->isActive()) {
            QPlatformNativeInterface *native = qApp->platformNativeInterface();
            if (!native) {
                return;
            }
            auto seat = static_cast<struct ::wl_seat *>(native->nativeResourceForIntegration("wl_seat"));
            if (!seat) {
                return;
            }

            mSelectionDevice.reset(new PrimarySelectionDevice(mSelectionManager->get_device(seat)));

            connect(mSelectionDevice.get(), &PrimarySelectionDevice::receivedSelectionChanged, this, [this]() {
                emit changed(QClipboard::Clipboard);
            });
            connect(mSelectionDevice.get(), &PrimarySelectionDevice::selectionChanged, this, [this]() {
                emit changed(QClipboard::Clipboard);
            });
        } else {
            mSelectionDevice.reset();
        }
    });
}

void WaylandClipboard::setMimeData(QMimeData *mime, QClipboard::Mode mode) {

    qDebug() << "WaylandClipboard::setMimeData(...)";
    if ( !mDataDevice and !mSelectionDevice ) {
        return;
    }

    if ( mode == QClipboard::Clipboard ) {
        auto source = std::make_unique<DataControlSource>(mDataManager->create_data_source(), mime);
        mDataDevice->setSelection( std::move( source ) );
        qDebug() << "Setting clipboard";
    }

    else if ( mode == QClipboard::Selection ) {
        auto source = std::make_unique<PrimarySelectionSource>(mSelectionManager->create_source(), mime);
        mSelectionDevice->setSelection( std::move( source ) );
        qDebug() << "Setting selection";
    }
}

void WaylandClipboard::clear( QClipboard::Mode mode ) {

    if (!mDataDevice) {
        return;
    }

    if (mode == QClipboard::Clipboard) {
        mDataDevice->set_selection(nullptr);
    }

    else if (mode == QClipboard::Selection) {
        if (zwlr_data_control_device_v1_get_version(mDataDevice->object()) >= ZWLR_DATA_CONTROL_DEVICE_V1_SET_PRIMARY_SELECTION_SINCE_VERSION) {
            mDataDevice->set_primary_selection(nullptr);
        }
    }
}

const QMimeData *WaylandClipboard::mimeData(QClipboard::Mode mode) const {

    if (!mDataDevice) {
        return nullptr;
    }

    if (mode == QClipboard::Clipboard) {
        // return our locally set selection if it's not cancelled to avoid copying data to ourselves
        return mDataDevice->selection() ? mDataDevice->selection() : mDataDevice->receivedSelection();
    }

    else if (mode == QClipboard::Clipboard) {
        // return our locally set selection if it's not cancelled to avoid copying data to ourselves
        return mSelectionDevice->selection() ? mSelectionDevice->selection() : mSelectionDevice->receivedSelection();
    }

    return nullptr;
}
