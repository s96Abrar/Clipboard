/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * Original work by David Edmundson <davidedmundson@kde.org>
    * This file was taken from plasma-workspace project
    * Suitable modifications were done to suit the needs of this project
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once
#include <memory>
#include "Global.hpp"

class DataControlDevice;
class DataControlDeviceManager;

class PrimarySelectionDevice;
class PrimarySelectionManager;

class ClipboardManager : public QObject {
    Q_OBJECT;

    public:
        static ClipboardManager* instance();

        virtual void setMimeData( QMimeData *mime, QClipboard::Mode mode ) = 0;
        virtual void clear( QClipboard::Mode mode ) = 0;
        virtual const QMimeData *mimeData( QClipboard::Mode mode ) const = 0;

    protected:
        ClipboardManager( QObject *parent );

    Q_SIGNALS:
        void changed( QClipboard::Mode mode );
};

class WaylandClipboard : public ClipboardManager {

    public:
        WaylandClipboard( QObject *parent );

        void setMimeData( QMimeData *mime, QClipboard::Mode mode ) override;
        void clear( QClipboard::Mode mode ) override;
        const QMimeData *mimeData( QClipboard::Mode mode ) const override;

    private:
        std::unique_ptr<DataControlDeviceManager> mDataManager;
        std::unique_ptr<DataControlDevice> mDataDevice;

        std::unique_ptr<PrimarySelectionManager> mSelectionManager;
        std::unique_ptr<PrimarySelectionDevice> mSelectionDevice;
};
