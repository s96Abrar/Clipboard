/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * Original work by David Edmundson <davidedmundson@kde.org>
    * This file was taken from plasma-workspace project
    * Suitable modifications were done to suit the needs of this project
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QApplication>
#include <QDebug>
#include "PrimarySelection.hpp"

QVariant PrimarySelectionOffer::retrieveData(const QString &mimeType, QVariant::Type type) const {

    if ( !hasFormat( mimeType ) )
        return QVariant();

    Q_UNUSED(type);

    int pipeFds[2];
    if (pipe(pipeFds) != 0) {
        return QVariant();
    }

    auto t = const_cast<PrimarySelectionOffer *>(this);
    t->receive(mimeType, pipeFds[1]);

    close(pipeFds[1]);

    /*
     * Ideally we need to introduce a non-blocking QMimeData object
     * Or a non-blocking constructor to QMimeData with the mimetypes that are relevant
     *
     * However this isn't actually any worse than X.
     */

    QPlatformNativeInterface *native = qApp->platformNativeInterface();
    auto display = static_cast<struct ::wl_display *>(native->nativeResourceForIntegration("wl_display"));
    wl_display_flush(display);

    QFile readPipe;
    if (readPipe.open(pipeFds[0], QIODevice::ReadOnly)) {
        QByteArray data;
        if (readData(pipeFds[0], data)) {
            return data;
        }
        close(pipeFds[0]);
    }

    return QVariant();
};

// reads data from a file descriptor with a timeout of 1 second
// true if data is read successfully
bool PrimarySelectionOffer::readData( int fd, QByteArray &data ) {

    fd_set readset;
    FD_ZERO(&readset);
    FD_SET(fd, &readset);
    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    for( ; ; ) {
        int ready = select( FD_SETSIZE, &readset, nullptr, nullptr, &timeout );
        if (ready < 0) {
            qWarning() << "PrimarySelectionOffer: select() failed";
            return false;
        }

        else if (ready == 0) {
            qWarning("PrimarySelectionOffer: timeout reading from pipe");
            return false;
        }

        else {
            char buf[4096];
            int n = read(fd, buf, sizeof buf);

            if (n < 0) {
                qWarning("PrimarySelectionOffer: read() failed");
                return false;
            }

            else if (n == 0) {
                return true;
            }

            else if (n > 0) {
                data.append(buf, n);
            }
        }
    }
}

PrimarySelectionSource::PrimarySelectionSource(struct ::zwp_primary_selection_source_v1 *id, QMimeData *mimeData) : QtWayland::zwp_primary_selection_source_v1(id) , m_mimeData(mimeData) {
    for (const QString &format : mimeData->formats()) {
        offer(format);
    }
}

void PrimarySelectionSource::zwp_primary_selection_source_v1_send(const QString &mime_type, int32_t fd) {

    QFile c;

    if (c.open(fd, QFile::WriteOnly, QFile::AutoCloseHandle)) {
        c.write(m_mimeData->data(mime_type));
        c.close();
    }
}

void PrimarySelectionSource::zwp_primary_selection_source_v1_cancelled() {

    Q_EMIT cancelled();
}

void PrimarySelectionDevice::setSelection(std::unique_ptr<PrimarySelectionSource> selection) {

    m_selection = std::move(selection);
    connect(m_selection.get(), &PrimarySelectionSource::cancelled, this, [this]() {
        m_selection.reset();
        Q_EMIT selectionChanged();
    });
    set_selection(m_selection->object(), 0);
    Q_EMIT selectionChanged();
}
