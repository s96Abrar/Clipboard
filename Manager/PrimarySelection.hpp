/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * Original work by David Edmundson <davidedmundson@kde.org>
    * This file was taken from plasma-workspace project
    * Suitable modifications were done to suit the needs of this project
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QFile>
#include <QMimeData>
#include <QtWaylandClient/QWaylandClientExtension>
#include <qpa/qplatformnativeinterface.h>

#include <unistd.h>
#include <memory>

#include "qwayland-wp-primary-selection-unstable-v1.h"

class PrimarySelectionManager : public QWaylandClientExtensionTemplate<PrimarySelectionManager>, public QtWayland::zwp_primary_selection_device_manager_v1 {
    Q_OBJECT;

    public:
        PrimarySelectionManager() : QWaylandClientExtensionTemplate<PrimarySelectionManager>( 1 ) {
        }

        ~PrimarySelectionManager() {
            destroy();
        }
};

class PrimarySelectionOffer : public QMimeData, public QtWayland::zwp_primary_selection_offer_v1 {
    Q_OBJECT;

    public:
        PrimarySelectionOffer(struct ::zwp_primary_selection_offer_v1 *id) : QtWayland::zwp_primary_selection_offer_v1(id) {
        }

        ~PrimarySelectionOffer() {

            destroy();
        }

        QStringList formats() const override {

            return m_receivedFormats;
        }

        bool hasFormat(const QString &format) const override {

            return m_receivedFormats.contains(format);
        }

    protected:
        void zwp_primary_selection_offer_v1_offer(const QString &mime_type) override {

            m_receivedFormats << mime_type;
        }

        QVariant retrieveData(const QString &mimeType, QVariant::Type type) const override;

    private:
        static bool readData(int fd, QByteArray &data);
        QStringList m_receivedFormats;
};

class PrimarySelectionSource : public QObject, public QtWayland::zwp_primary_selection_source_v1 {
    Q_OBJECT;

    public:
        PrimarySelectionSource(struct ::zwp_primary_selection_source_v1 *id, QMimeData *mimeData);
        PrimarySelectionSource();

        ~PrimarySelectionSource() {

            destroy();
        }

        QMimeData *mimeData() {

            return m_mimeData;
        }

    Q_SIGNALS:
        void cancelled();

    protected:
        void zwp_primary_selection_source_v1_send(const QString &mime_type, int32_t fd) override;
        void zwp_primary_selection_source_v1_cancelled() override;

    private:
        QMimeData *m_mimeData;
};

class PrimarySelectionDevice : public QObject, public QtWayland::zwp_primary_selection_device_v1 {

    Q_OBJECT;
    public:
        PrimarySelectionDevice(struct ::zwp_primary_selection_device_v1 *id) : QtWayland::zwp_primary_selection_device_v1(id) {
        }

        ~PrimarySelectionDevice() {
            destroy();
        }

        void setSelection(std::unique_ptr<PrimarySelectionSource> selection);

        QMimeData *receivedSelection() {

            return m_receivedSelection.get();
        }

        QMimeData *selection() {

            return m_selection ? m_selection->mimeData() : nullptr;
        }

    Q_SIGNALS:
        void receivedSelectionChanged();
        void selectionChanged();

    protected:
        void zwp_primary_selection_device_v1_data_offer(struct ::zwp_primary_selection_offer_v1 *id) override {
            new PrimarySelectionOffer(id);
            // this will become memory managed when we retrieve the selection event
            // a compositor calling data_offer without doing that would be a bug
        }

        void zwp_primary_selection_device_v1_selection(struct ::zwp_primary_selection_offer_v1 *id) override {
            if (!id) {
                m_receivedSelection.reset();
            }

            else {
                auto deriv = QtWayland::zwp_primary_selection_offer_v1::fromObject(id);
                auto offer = dynamic_cast<PrimarySelectionOffer *>(deriv); // dynamic because of the dual inheritance
                m_receivedSelection.reset(offer);
            }
            emit receivedSelectionChanged();
        }

    private:
        std::unique_ptr<PrimarySelectionSource> m_selection; // selection set locally
        std::unique_ptr<PrimarySelectionOffer> m_receivedSelection; // latest selection set from externally to here
};
