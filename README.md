# DesQ Clipboard

A simple clipboard for DesQ Shell.

### Notes for compiling (Qt5) - linux:

- Get the sources
  - `git clone https://gitlab.com/DesQ/DesQUtils/Clipboard`
- Enter the `Clipboard` and create and enter `build`.
  - `cd Clipboard && mkdir build && cd build`
- Configure the project - we use cmake for project management
  - `cmake ..`
- Compile and install - we use make
  - `make -kj$(nproc) && sudo make install`

### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools, libqt5waylandclient5-dev)
* wayland (libwayland-dev, wayland-protocols)
* wlroots (Preferably compiled from git)

## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.1
* wayland:          1.18.0-2~exp1.1
* wlroots:          0.12.0-dc61f471

### Known Bugs
* Please test and let me know

### Upcoming
* Any other feature you request for... :)
