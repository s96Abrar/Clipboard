/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "Clipboard.hpp"

Clipboard::Clipboard( DesQWaylandRegistry *reg ) : QWidget() {

    /* Wayland DesQRegistry */
    registry = reg;

    /* Title */
	setWindowTitle( "DesQ Clipboard" );

	/* Set the windowFlags */
	setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::BypassWindowManagerHint );

    /* Clipboard manager */
    clMgr = ClipboardManager::instance();
    connect( clMgr, &ClipboardManager::changed, this, &Clipboard::updateClipboard );
    qDebug() << "Clipboard Manager init complete";

    /* Clipboard History */
    history = new History();
    qDebug() << "Loading saved history";

    /* Minimum window size */
    setFixedSize( QSize( 360, 450 ) );

    /* Create a base GUI */
    QScrollArea *scroll = new QScrollArea( this );
    scroll->setWidgetResizable( true );
    scroll->setFrameStyle( QFrame::NoFrame );
    scroll->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( scroll );
    setLayout( lyt );

    QWidget *base = new QWidget();
    baseLyt = new QHBoxLayout();
    base->setLayout( baseLyt );
    scroll->setWidget( base );
    qDebug() << "Minimal UI init";

    /* Load the history items */
    loadClipboardItems();
    qDebug() << "Loading history items";

    /* Restore the last clipboard content as the current */
    setClipboard( 0, QClipboard::Clipboard );
};

Clipboard::~Clipboard() {

    /* Results in saving the history if it's enabled */
    delete history;
};

void Clipboard::startTray() {

    QSystemTrayIcon *icon = new QSystemTrayIcon( QIcon( ":/icons/desq-clipboard.png" ), this );
    icon->show();

    connect(
        icon, &QSystemTrayIcon::activated, [=]( QSystemTrayIcon::ActivationReason reason ) {
            switch( reason ) {
                case QSystemTrayIcon::Trigger: {
                    if ( isVisible() )
                        hide();

                    else
                        show();

                    break;
                }

                case QSystemTrayIcon::Context: {
                    break;
                }

                case QSystemTrayIcon::MiddleClick: {
                    qApp->quit();
                    break;
                }

                case QSystemTrayIcon::DoubleClick: {
                    break;
                }

                case QSystemTrayIcon::Unknown: {
                    break;
                }
            }
        }
    );
};

void Clipboard::loadClipboardItems() {

    /* Clear the clipItems list */
    qDeleteAll( clipItems );
    clipItems.clear();

    /* Delete the previously added ClipboardItem */
    if ( baseLyt->count() )
        qDeleteAll( baseLyt->findChildren<ClipboardItem*>() );

    /* Delete the previously added QVBoxLayout */
    if ( baseLyt->count() )
        qDeleteAll( baseLyt->findChildren<QVBoxLayout*>() );

    QVBoxLayout *left = new QVBoxLayout();
    QVBoxLayout *right = new QVBoxLayout();

    for( uint i = 0; i < history->count(); i++ ) {
        ClipboardItem *item = new ClipboardItem( i, history->itemAt( i ), this );
        if ( i % 2 == 0 )
            left->addWidget( item );

        else
            right->addWidget( item );

        clipItems.append( item );
        connect(
            item, &ClipboardItem::clicked, [=]( uint i ) {
                for( ClipboardItem *cItem: clipItems ) {
                    if ( cItem->index() != i )
                        cItem->deselect();
                }

                setClipboard( i, QClipboard::Clipboard );
            }
        );
        qDebug() << "Added history item" << i;
    }

    left->addStretch();
    right->addStretch();

    baseLyt->addLayout( left );
    baseLyt->addLayout( right );
};

void Clipboard::updateClipboard( QClipboard::Mode mode ) {

    /* In cases like spinbox selection, we can ignore the selection */
    // if ( ignoreClipboardChanges() ) {
        // if ( history->count() )
            // setClipboard( 0, mode ? QClipboard::Selection : QClipboard::Clipboard );

        // return;
    // }

	bool selectionMode = (mode == QClipboard::Selection);
	QMimeData *data = (QMimeData *)clMgr->mimeData( selectionMode ? QClipboard::Selection : QClipboard::Clipboard );

	/* The klipper portion of the code works for empty clipboard
	 * Such as you opened the clipboard manager but based on your previous history
	 * this will select the top item of the history
	*/

	/* This portion of the code is collected from klipper */
	bool clipEmpty = false;
	bool changed = true;
	if (!data) {
		clipEmpty = true;
	} else {
		clipEmpty = data->formats().isEmpty();
		if (clipEmpty) {
			clipEmpty = data->formats().isEmpty();
			qDebug() << "was empty. Retried, now " << (clipEmpty ? " still empty" : " no longer empty");
		}
	}

	if (changed && clipEmpty) {
		auto top = history->itemAt(0);
		if (top) {
			qDebug() << "Resetting clipboard (Prevent empty clipboard)";
			int selMod = selectionMode ? QClipboard::Selection : QClipboard::Clipboard;
			if (selMod & QClipboard::Selection) {
				qDebug() << "Setting selection to <" << top->text() << ">";
				QMimeData *mimeData = top;
				mimeData->setData(QStringLiteral("application/x-kde-onlyReplaceEmpty"), "1");
				clMgr->setMimeData(mimeData, QClipboard::Selection);
			}

			if (selMod & QClipboard::Clipboard) {
				qDebug() << "Setting clipboard to <" << top->text() << ">";
				QMimeData *mimeData = top;
				mimeData->setData(QStringLiteral("application/x-kde-onlyReplaceEmpty"), "1");
				clMgr->setMimeData(mimeData, QClipboard::Clipboard);
			}
		}
		return;
	} else if (clipEmpty) {
		return;
	}
	/* =================================================== */

	QMimeData *newdata = new QMimeData;
	newdata->setText(data->text());
	history->addItem(newdata);

	/* This portion of the code is collected from klipper */
	if (changed) {
		int selMod = selectionMode ? QClipboard::Selection : QClipboard::Clipboard;
		if (selMod & QClipboard::Selection) {
			qDebug() << "Setting selection to <" << newdata->text() << ">";
			QMimeData *mimeData = newdata;
			mimeData->setData(QStringLiteral("application/x-kde-onlyReplaceEmpty"), "1");
			clMgr->setMimeData(mimeData, QClipboard::Selection);
		}

		if (selMod & QClipboard::Clipboard) {
			qDebug() << "Setting clipboard to <" << newdata->text() << ">";
			QMimeData *mimeData = newdata;
			mimeData->setData(QStringLiteral("application/x-kde-onlyReplaceEmpty"), "1");
			clMgr->setMimeData(mimeData, QClipboard::Clipboard);
		}

		/*********************** There is another problem with this loading *************/
		loadClipboardItems();
		/*********************** Each item opening the UI adds the last clipboard item *************/
	}
	/* =================================================== */


	/*
	 * This is the initial solution for the crash
	 *
	 * The reason is the collected mimedata pointer somehow freed from the memory
	 * Hence while saving the items in the history class we end up not finiding that pointer
	 * and having SEGFAULT
	 * So creating a new mime data pointer and saving it solves the problem.
	*/
//	if (data) {
//		if (!data->formats().isEmpty()) {
//			if (data->hasText()) {
//				QMimeData *newdata = new QMimeData;
//				newdata->setText(data->text());
//				history->addItem( newdata );

//				loadClipboardItems();
//				clMgr->setMimeData(newdata, selectionMode ? QClipboard::Clipboard : QClipboard::Selection);
//			}
//		}
//	}

};

bool Clipboard::ignoreClipboardChanges() const {

    QWidget *focusWidget = qApp->focusWidget();
    if (focusWidget) {
        if (focusWidget->inherits("QSpinBox")
            || (focusWidget->parentWidget() && focusWidget->inherits("QLineEdit") && focusWidget->parentWidget()->inherits("QSpinWidget"))) {
            return true;
        }
    }

    return false;
};

void Clipboard::setClipboard( int idx, QClipboard::Mode mode ) {

    if ( ( not history->count() ) or ( history->count() < (uint)idx ) ) {
        qDebug() << "Empty clipboard";
        return;
    }

    history->moveItemToTop( idx );
    QMimeData *data = history->itemAt( 0 );

    switch( mode ) {
        case  QClipboard::Selection: {
            qDebug() << "Setting selection to <" << data->text() << ">";
            clMgr->setMimeData( data, QClipboard::Selection );
            break;
        }

        case QClipboard::Clipboard: {
            qDebug() << "Setting clipboard to <" << data->text() << ">";
            clMgr->setMimeData( data, QClipboard::Clipboard );
            break;
        }

        default: {
            break;
        }
    }

    loadClipboardItems();
};

void Clipboard::show() {

    QWidget::show();

    /* Show as a layer surface */
	DesQLayerSurface *cls = new DesQLayerSurface( this->windowHandle() );
	cls->setup( registry->layerShell(), DesQLayerShell::Overlay );
	cls->setSurfaceSize( this->size() );
	cls->setExclusiveZone( -1 );
	cls->setKeyboardInteractivity( DesQLayerSurface::Exclusive );
	cls->apply();
};

void Clipboard::keyPressEvent( QKeyEvent *kEvent ) {

    if ( kEvent->key() == Qt::Key_Escape )
        hide();

    QWidget::keyPressEvent( kEvent );
};

ClipboardItem::ClipboardItem( int index, QMimeData *data, QWidget *parent ) : QLabel( parent ) {

    setMargin( 10 );
    setWordWrap( true );

    setFixedWidth( 160 );
    setMaximumHeight( maxHeight );

    mIndex = index;

    setToolTip( data->formats().join( "\n" ) );

    if ( data->hasText() ) {
        setAlignment( Qt::AlignLeft );
        setText( data->text() );
    }

    else if ( data->hasHtml() ) {
        setAlignment( Qt::AlignLeft );
        setText( data->html() );
    }

    else if ( data->formats().filter( "text/" ).count() ) {
        setAlignment( Qt::AlignLeft );
        setText( data->text() );
    }

    else if ( data->hasImage() ) {
        setAlignment( Qt::AlignCenter );
        QPixmap pix = QPixmap::fromImage( qvariant_cast<QImage>( data->imageData() ) );
        if ( pix.isNull() )
            setPixmap( QIcon::fromTheme( "image-x-generic" ).pixmap( 64 ) );

        else
            setPixmap( pix.scaled( 160, maxHeight, Qt::KeepAspectRatioByExpanding ) );
    }

    else if ( data->formats().contains( "text/uri-list" ) ) {
        QStringList uriList = QString::fromUtf8( data->data( "text/uri-list" ) ).trimmed().split( "\n" );
        QString newList = "\u1F5CE" + uriList.join( "\u1F5CE" );

        setText( newList );
    }

    else {
        setAlignment( Qt::AlignCenter );
        QPixmap pix;
        for( QString fmt: data->formats() ) {
            if ( QIcon::hasThemeIcon( fmt.replace( "/", "-" ) ) ) {
                pix = QIcon::fromTheme( fmt.replace( "/", "-" ) ).pixmap( 64 );
                break;
            }
        }

        if ( pix.isNull() )
            pix = QIcon::fromTheme( "application-octet-stream" ).pixmap( 64 );

        setPixmap( pix );
    }

    setMouseTracking( true );
};

uint ClipboardItem::index() {

    return mIndex;
};

void ClipboardItem::select() {

    selected = true;
    repaint();
};

void ClipboardItem::deselect() {

    selected = false;
    repaint();
};

bool ClipboardItem::isSelected() {

    return selected;
};

void ClipboardItem::enterEvent( QEvent *event ) {

    hovered = true;
    repaint();
};

void ClipboardItem::leaveEvent( QEvent *event ) {

    hovered = false;
    repaint();
};

void ClipboardItem::mousePressEvent( QMouseEvent *mEvent ) {

    if ( mEvent->button() == Qt::LeftButton ) {
        pressed = true;
        repaint();
    }

    QLabel::mousePressEvent( mEvent );
};

void ClipboardItem::mouseReleaseEvent( QMouseEvent *mEvent ) {

    if ( mEvent->button() == Qt::LeftButton ) {
        pressed = false;
        select();

        emit clicked( mIndex );
    }

    QLabel::mouseReleaseEvent( mEvent );
};

void ClipboardItem::paintEvent( QPaintEvent *pEvent ) {

    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );

    QColor highlight = palette().color( QPalette::Highlight );
    QColor background = palette().color( QPalette::AlternateBase );

    if ( selected ) {
        /* Background */
        highlight.setAlphaF( 0.5 );
        painter.setPen( Qt::NoPen );
        painter.setBrush( highlight );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );

        /* Border */
        highlight.setAlphaF( 1.0 );
        painter.setPen( highlight );
        painter.setBrush( Qt::NoBrush );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );
    }

    else if ( hovered ) {
        /* Background */
        highlight.setAlphaF( 0.3 );
        painter.setPen( Qt::NoPen );
        painter.setBrush( highlight );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );

        /* Border */
        highlight.setAlphaF( 0.5 );
        painter.setPen( highlight );
        painter.setBrush( Qt::NoBrush );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );
    }

    else if ( pressed ) {
        /* Background */
        highlight.setAlphaF( 0.7 );
        painter.setPen( Qt::NoPen );
        painter.setBrush( highlight );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );

        /* Border */
        highlight.setAlphaF( 1.0 );
        painter.setPen( highlight );
        painter.setBrush( Qt::NoBrush );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );
    }

    else {
        /* Background */
        background.setAlphaF( 0.2 );
        painter.setPen( Qt::NoPen );
        painter.setBrush( background );
        painter.drawRoundedRect( rect().adjusted( 1, 1, -1, -1 ), 3.0, 3.0 );
    }

    painter.end();

    QLabel::paintEvent( pEvent );

    /* Draw a 'x' button at the corner to delete the entry */
    painter.begin( this );
};
