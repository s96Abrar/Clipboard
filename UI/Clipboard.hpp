/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "Global.hpp"
#include "History.hpp"
#include "ClipboardManager.hpp"

#include <desqwl/DesQWlGlobal.hpp>
#include <desqwl/DesQRegistry.hpp>
#include <desqwl/DesQLayerShell.hpp>
#include <desqwl/DesQHotSpot.hpp>

class ClipboardItem;

class Clipboard : public QWidget {

    Q_OBJECT;

    public:
        Clipboard( DesQWaylandRegistry *reg );
        virtual ~Clipboard();

        void startTray();

    private:
        ClipboardManager *clMgr;
        History *history;

        QHBoxLayout *baseLyt;
        QList<ClipboardItem *> clipItems;

        DesQWaylandRegistry *registry;

        void loadClipboardItems();

        void updateClipboard( QClipboard::Mode mode );
        bool ignoreClipboardChanges() const;

        void setClipboard( int idx, QClipboard::Mode mode );

    public Q_SLOTS:
        void show();

    protected:
        void keyPressEvent( QKeyEvent *kEvent );
};

class ClipboardItem : public QLabel {
    Q_OBJECT;

    public:
        ClipboardItem( int index, QMimeData *data, QWidget *parent );

        uint index();

        void select();
        void deselect();
        bool isSelected();

    private:
        QMimeData *mData;

        uint mIndex;

        const int maxHeight = 100;

        bool pressed = false;
        bool hovered = false;
        bool selected = false;

    Q_SIGNALS:
        void clicked( uint );

    protected:
        void enterEvent( QEvent *event ) override;
        void leaveEvent( QEvent *event ) override;

        void mousePressEvent( QMouseEvent *mEvent ) override;
        void mouseReleaseEvent( QMouseEvent *mEvent ) override;

        void paintEvent( QPaintEvent *pEvent ) override;
};
